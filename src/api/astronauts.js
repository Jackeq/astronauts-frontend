import axios from "axios";

export default axios.create({
  baseURL: "https://astronauts-jack.herokuapp.com/api/v1",
});
