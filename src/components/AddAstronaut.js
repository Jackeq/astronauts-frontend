import React from "react";

class AddAstronaut extends React.Component {
  state = {
    name: "",
    surname: "",
    birthdate: "",
    superpower: "",
  };

  add = (e) => {
    e.preventDefault();
    if (this.state.name === "" || this.state.surname === "" || this.state.birthdate === "" || this.state.superpower === "") {
      alert("ALl the fields are mandatory!");
      return;
    }
    this.props.addAstronautHandler(this.state);
    this.setState({ name: "", surname: "", birthdate: "", superpower: ""  });
    this.props.history.push("/");
  };

  render() {
    return (
      <div className="ui main">
        <h2>Add Astronaut</h2>
        <form className="ui form" onSubmit={this.add}>
          <div className="field">
            <label>Name</label>
            <input
              type="text"
              name="name"
              placeholder="Name"
              value={this.state.name}
              onChange={(e) => this.setState({ name: e.target.value })}
            />
          </div>
          <div className="field">
            <label>Surname</label>
            <input
              type="text"
              name="surname"
              placeholder="Surname"
              value={this.state.surname}
              onChange={(e) => this.setState({ surname: e.target.value })}
            />
          </div>
          <div className="field">
            <label>Birthdate</label>
            <input
                type="date"
                name="birthdate"
                placeholder="Birthdate"
                value={this.state.birthdate}
                onChange={(e) => this.setState({ birthdate: e.target.value })}
            />
          </div>
          <div className="field">
            <label>Superpower</label>
            <input
                type="text"
                name="superpower"
                placeholder="Superpower"
                value={this.state.superpower}
                onChange={(e) => this.setState({ superpower: e.target.value })}
            />
          </div>
          <button className="ui button blue">Add</button>
        </form>
      </div>
    );
  }
}

export default AddAstronaut;
