import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import api from "../api/astronauts";
import "./App.css";
import Header from "./Header";
import AddAstronaut from "./AddAstronaut";
import AstronautList from "./AstronautList";
import EditAstronaut from "./EditAstronaut";

function App() {
  /*const LOCAL_STORAGE_KEY = "astronauts";*/
  const [astronauts, setAstronauts] = useState([]);

  //RetrieveContacts
  const retrieveAstronauts = async () => {
    const response = await api.get("/Astronaut");
    return response.data;
  };

  const addAstronautHandler = async (astronaut) => {
    console.log(astronaut);
    const request = {
      ...astronaut,
    };

    const response = await api.post("/Astronaut", request);
    console.log(response);
    setAstronauts([...astronauts, response.data]);
  };

  const updateAstronautHandler = async (astronaut) => {
    const response = await api.put(`/Astronaut/${astronaut.id}`, astronaut);
    const { id, name, surname, birthdate, superpower } = response.data;
    setAstronauts(
      astronauts.map((astronaut) => {
        return astronaut.id === id ? { ...response.data } : astronaut;
      })
    );
  };

  const removeAstronautHandler = async (id) => {
    await api.delete(`/Astronaut/${id}`);
    const newAstronautList = astronauts.filter((astronaut) => {
      return astronaut.id !== id;
    });

    setAstronauts(newAstronautList);
  };

  useEffect(() => {
    const getAllAstronauts = async () => {
      const allAstronauts = await retrieveAstronauts();
      if (allAstronauts) setAstronauts(allAstronauts);
    };

    getAllAstronauts();
  }, []);

  return (
    <div className="ui container">
      <Router>
        <Header />
        <Switch>
          <Route
            path="/"
            exact
            render={(props) => (
              <AstronautList
                {...props}
                astronauts={astronauts}
                getAstronautId={removeAstronautHandler}
              />
            )}
          />
          {<Route
            path="/add"
            render={(props) => (
              <AddAstronaut {...props} addAstronautHandler={addAstronautHandler} />
            )}
          />}

          {<Route
            path="/edit"
            render={(props) => (
              <EditAstronaut
                {...props}
                updateAstronautHandler={updateAstronautHandler}
              />
            )}
          />}

        </Switch>
      </Router>
    </div>
  );
}

export default App;
