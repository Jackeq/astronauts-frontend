import React from "react";
import { Link } from "react-router-dom";
import user from "../images/user.png";

const AstronautCard = (props) => {
  const { id, name, surname, birthdate, superpower } = props.astronaut;
  return (
    <div className="item">
      <img className="ui avatar image" src={user} alt="user" />
      <div className="content">

            <div><span className="violet-text">Jméno:</span> <span className="black-text">{name} {surname}</span></div>
            <div><span className="violet-text">Datum narození:</span> <span className="black-text">{new Intl.DateTimeFormat("cs-CZ", {
              year: "numeric",
              month: "long",
              day: "2-digit"
          }).format(new Date(birthdate))}</span></div>
            <div><span className="violet-text">Super-schopnost:</span> <span className="black-text">{superpower}</span></div>
      </div>
      <i
        className="trash alternate outline icon"
        style={{ color: "red", marginTop: "7px", marginLeft: "10px" }}
        onClick={() => props.clickHander(id)}
      ></i>
      {<Link to={{ pathname: `/edit`, state: { astronaut: props.astronaut } }}>
        <i
          className="edit alternate outline icon"
          style={{ color: "blue", marginTop: "7px" }}
        ></i>
      </Link>}
    </div>
  );
};

export default AstronautCard;
