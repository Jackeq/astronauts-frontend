import React from "react";
import { Link } from "react-router-dom";
import AstronautCard from "./AstronautCard";

const AstronautList = (props) => {
  console.log(props);

  const deleteAstronautHandler = (id) => {
    props.getAstronautId(id);
  };

  const renderAstronautList = props.astronauts.map((astronaut) => {
    return (
      <AstronautCard
        astronaut={astronaut}
        clickHander={deleteAstronautHandler}
        key={astronaut.id}
      />
    );
  });
  return (
    <div className="main">
      <h2>
        Astronaut List
        <Link to="/add">
          <button className="ui button blue right">Add Astronaut</button>
        </Link>
      </h2>
      <div className="ui celled list">{renderAstronautList}</div>
    </div>
  );
};

export default AstronautList;
