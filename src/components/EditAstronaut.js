import React from "react";

class EditAstronaut extends React.Component {
  constructor(props) {
    super(props);
    const { id, name, surname, birthdate, superpower } = props.location.state.astronaut;
    this.state = {
      id,
      name,
      surname,
      birthdate,
      superpower,
    };
  }

  update = (e) => {
    e.preventDefault();
    if (this.state.name === "" || this.state.surname === "" || this.state.birthdate === "" || this.state.superpower === "") {
      alert("ALl the fields are mandatory!");
      return;
    }
    this.props.updateAstronautHandler(this.state);
    this.setState({ name: "", surname: "", birthdate: "", superpower: "" });
    this.props.history.push("/");
  };
  render() {
    return (
      <div className="ui main">
        <h2>Edit Astronaut</h2>
        <form className="ui form" onSubmit={this.update}>
          <div className="field">
            <label>Name</label>
            <input
              type="text"
              name="name"
              placeholder="Name"
              value={this.state.name}
              onChange={(e) => this.setState({ name: e.target.value })}
            />
          </div>
          <div className="field">
            <label>Surname</label>
            <input
              type="text"
              name="surname"
              placeholder="Surname"
              value={this.state.surname}
              onChange={(e) => this.setState({ surname: e.target.value })}
            />
          </div>
          <div className="field">
            <label>Birthdate</label>
            <input
                type="date"
                name="birthdate"
                placeholder="Birthdate"
                value={this.state.birthdate}
                onChange={(e) => this.setState({ birthdate: e.target.value })}
            />
          </div>
          <div className="field">
            <label>Superpower</label>
            <input
                type="text"
                name="superpower"
                placeholder="Superpower"
                value={this.state.superpower}
                onChange={(e) => this.setState({ superpower: e.target.value })}
            />
          </div>
          <button className="ui button blue">Update</button>
        </form>
      </div>
    );
  }
}

export default EditAstronaut;
